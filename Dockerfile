# minimal dockerfile for jubbr lua api
FROM docker:git
MAINTAINER istr <docker@ingostruck.de>

# Ensure UTF-8
ENV LANG       en_US.UTF-8
ENV LC_ALL     en_US.UTF-8

# Change this ENV variable to skip the docker cache from this line on
ENV LATEST_CACHE 2016-09-17T12:00-00:00

COPY "base.tar.gz" "/var/tmp/base.tar.gz"

USER root
RUN mkdir -p /opt/jc
RUN cd /opt/jc && gunzip -c /var/tmp/base.tar.gz | tar xvf -
RUN id -g jc &>/dev/null || addgroup -g 889 jc
RUN id -u jc &>/dev/null || adduser -G jc -u 889 -s /bin/bash -h /opt/jc/ -H -D jc
RUN chown -R jc:jc /opt/jc
RUN chown -R root:root /opt/jc/root

# update
RUN apk update

# add packages that are stripped from docker:git
RUN apk add --no-cache alpine-base alpine-conf libcom_err popt openntpd

# add base libs
RUN apk add --no-cache libgcc libstdc++ zlib openssl pcre readline ncurses yaml python tzdata

# add base tools
RUN apk add --no-cache sed coreutils diffutils unzip iptables blkid hdparm

# add dev tools and libs
RUN apk add --no-cache --virtual .build-deps docker help2man alpine-sdk linux-headers paxmark libtool m4 automake autoconf tcl musl-dev zlib-dev openssl-dev pcre-dev readline-dev ncurses-dev yaml-dev postgresql-dev

RUN rm -rf /usr/share/man/
RUN rm -rf /usr/share/info/
RUN rm -rf /var/cache/apk/*
